//require de express et cors
const express = require('express');
const cors = require('cors');

//instantiation de sxpress
const app = express();

//parser le body
app.use(express.json() );
app.use( express.urlencoded({extended: true}) );

//test de routage sur la home ou /
app.get('/', (req, res) =>{
    res.send('Bonjour');
}  );

app.listen( 8080 , ()=> { console.log('serveur demmaré') } );

//pour le test : http://localhost:8080