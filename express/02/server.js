const express = require("express");
const cors = require("cors"); //middleware de express

let app = express();

let corsOption = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOption));
//parser les request au content-type- appliquation/jason
app.use(express.json());

app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync();

//test de routage
app.get('/', (req, res) => {
    res.json({ prenom: "Bienvenue a mon application" });
});

const PORT = 8080;

app.listen(PORT, () => {
    console.log('le serveur demarre');
});

//http://localhost:8080 ou http://127.0.0.1:8080