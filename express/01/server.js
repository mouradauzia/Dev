let express = require('express');
let app = express();
let bodyParser = require('body-parser'); //middleware = interlogiciel

//urlencod form parser
let urlencodParser = bodyParser.urlencoded({ extended: false });

app.use(express.static("public"));

app.get('/', function (req, res) {
    res.send(' Hello GET homepage ');
    console.log('je suis dans la hompage')
});


app.get('/list_user', function (req, res) {
    console.log('je suis dans la liste des user');
    res.send(' Hello GET page liste user ');
});

app.get('/login', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
});

app.get('/process_get', function (req, res) {

    reponse = {
        nom: req.query.nom,
        prenom: req.query.prenom
    };

    console.log(reponse);
    res.end(JSON.stringify(reponse));
});

//action du formulaire par post
app.post('/process_post', urlencodParser, function (req, res) {
    reponse = {
        nom: req.body.nom,
        prenom: req.body.prenom
    };

    console.log(reponse);
    res.end(JSON.stringify(reponse));
});

let server = app.listen(8081, function () {
    let host = server.address().address;
    let port = server.address().port;

    console.log("exemple app qui ecoute sur l'adresse http://%s:%s", host, port);
});
