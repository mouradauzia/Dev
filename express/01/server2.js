let express = require('express');
let app = express();
let bodyParser = require('body-parser');

let urlencodParser = bodyParser.urlencoded({ extended: false });

app.use(express.static("public"));

app.get('/', function (req, res) {
    res.send('bonjour')
});


app.get('/user', function (req, res) {
    res.sendFile(__dirname + "/" + "user.html");
});

app.post('/valider', urlencodParser, function (req, res) {
    reponse = {
        nom: req.body.nom,
        prenom: req.body.prenom,
        tél: req.body.tél
    };

    console.log(reponse);
    res.end(JSON.stringify(reponse));
});

let server = app.listen(8081, function () {
    let host = server.address().address;
    let port = server.address().port;

    console.log("exemple app qui ecoute sur l'adresse http://%s:%s", host, port);
});