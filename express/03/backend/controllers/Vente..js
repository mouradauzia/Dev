import Product from "../models/VenteModels.js";

export const getAllProducts = async (req, res) => {
    try {
        const ventes = await Product.findAll();
        res.json(ventes);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const getventesById = async (req, res) => {
    try {
        const Vente = await Vente.findAll({
            where: {
                id: req.params.id
            }
        });
        res.json(Vente[0]);
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const createVente = async (req, res) => {
    try {
        await Vente.create(req.body);
        res.json({
            "message": "Vente Created"
        });
    } catch (error) {
        res.json({ message: error.message });
    }  
}

//http://localhost:5000/api/products/1
export const deleteProduct = async (req, res) => {
    try {
        await Product.destroy({
            where: {
                id: req.params.id
            }
        });
        res.json({ "message": "Produit supprimé" });
    } catch (error) {
        res.json({ message: error.message });
    }
}

export const updateProduct = async (req, res) => {
     console.log('test');

    try {
        await Product.update(req.body, {
            where: {
                id: req.params.id
            }
        });
        res.json({ "message": "Produit mis a jours" });
    } catch (error) {
        res.json({ message: error.message });
    }
}