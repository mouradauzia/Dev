import { Sequelize } from "sequelize";

const db = new Sequelize('mern_db', 'root', '' , {
    host: "127.0.0.1",
    port: 3388,
    dialect: "mysql"
} );

export default db;