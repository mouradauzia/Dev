import express from "express";
import db from "./config/database.js";
import productRoutes from './routes/index.js';
import cors from 'cors';

const app = express();

try {
    await db.authenticate();
    await db.sync();
    //await db.sync( {force: true} );
    //await db.sync( {alter: true} ); // TESTE SUR LA STRECTURE DE LA TABLE 5LES CHAMPS

    console.log('Connecté a la DB');
} catch (error) {
    console.error('erreur de connection : ', error)
}

app.use(cors());
app.use(express.json());
app.use('/api', productRoutes);

app.listen(5000, () => console.log('serveur demarré sur le port 5000'));

