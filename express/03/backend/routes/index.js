import express, { Router } from 'express';

import { getAllProducts, createProduct, deleteProduct, updateProduct, getProductById } from "../controllers/Products.js";
import { getventesById } from '../controllers/Vente..js';
import { getAllVentes } from '../controllers/Vente.js';

const router = express.Router();

router.get('/products/', getAllProducts);
router.get('/products/:id', getProductById);
router.post('/products/', createProduct);
router.delete('/:id', updateProduct);
router.patch('/:id', updateProduct);
router.get('/ventes', getAllVentes);
router.get('/ventes/:id' , getventesById);



export default router;


