let books;
function buttonClick() {
    //on cree une instance ou une copie de l'objet XMLHttpRequest
    let req = instanceXMLHttp();

    req.onreadystatechange = function () {

        // récupère la réponse du serveur
        if (req.readyState == 4 && req.status == 200) {
            let oXmldocument = req.responseXML;

            
        }
    }

    let sFileLoad = "data/books.xml"
    // le random pour éviter la mise en cache du navigateur
    req.open("GET", sFileLoad + "?rand=" + Math.random(), true);
    req.send(null);


}

function afficheCateg() {
    let odiv = document.getElementById("ulcat");
    odiv.innerHTML = "";
    for (i = 0; i <= books.length - 1; i++) {
        odiv.innerHTML += "<li onClick = 'detailLivre(" + i + ")'><a href= '#'><h3>" + books.item(i).getElementsByTagName("title")[0].firstChild.data + "</h3></a></li>";
    }

}

function detailLivre(indice) {

    let oprod = document.getElementById("prod");
    let iBook = books.item(indice);
    let titre = iBook.getElementsByTagName("title")[0].firstChild.data;
    let description = iBook.getElementsByTagName("description")[0].firstChild.data;
    let photo = iBook.getElementsByTagName("photo")[0].firstChild.data;
    let author = iBook.getElementsByTagName("author")[0].firstChild.data;
    let price = iBook.getElementsByTagName("price")[0].firstChild.data;

    oprod.innerHTML = titre + "<br>" + description + "<br><img src='" + photo + "' alt=''> <br>" + author + "<br>" + price;

}

function instanceXMLHttp() {
    if (window.XMLHttpRequest)// Pour les autres navigateurs (Mozilla, Firefox, Safari, Konqueror, Opéra, IE7)
    {
        req = new XMLHttpRequest();
        console.log("oui - (Mozilla, Firefox, Safari, Konqueror, Opéra, IE7 - xhr est un " + req);
    }
    else if (window.ActiveXObject)// Pour Internet Explorer (IE 5, IE 5.5, IE 6)
    {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        console.log("oui - IE 5, IE 5.5, IE 6 - xhr est un " + req);
    }
    else {
        alert("Votre navigateur n’est pas compatible avec AJAX...");
        return;
    }
    return req;
}