let req = new XMLHttpRequest();

let root = document.getElementById('root');
let logo = document.createElement('img');
logo.src = './img/logo.png';

root.appendChild(logo);

let container = document.createElement('div');
container.setAttribute('class', 'conteneur');
root.appendChild(container);

req.open('GET', 'https://ghibliapi.herokuapp.com/films', true);

req.onload = function () {

    let data = JSON.parse(this.response);

    if (req.status >= 200 && req.status < 400) {

        data.forEach((movie) => {
            //console.log(movie.title );
            let card = document.createElement('div');
            card.setAttribute('class', 'card');

            let image = document.createElement('img');
            image.src = movie.image ;

            let h1 = document.createElement('h2');
            h1.textContent = movie.title;

            let p = document.createElement('p');
            movie.description = movie.description.substring(0, 300);

            p.textContent = ` ${movie.description}...`;

            card.appendChild(image);
            card.appendChild(h1);
            container.appendChild(p);
            container.appendChild(card);
        });

    } else {
        console.log('erreur');
    }

}

req.send();