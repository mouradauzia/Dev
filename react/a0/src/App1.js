import React from "react";
import Content from "./components/Content";
import Pictures from "./components/Pictures";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { afficher: false, titre: "Afficher" };

  }

  componentDidMount() {
    this.setState({ titre: 'le composant à été monté' });
  }

  render() {
    return (
      <div>
        <h1 className="text-4xl text-purple-700 y-5" >{this.state.titre}</h1>
        <button className="bg-purple-900 text-white rounded py-2 px-3"
          onClick={() => {
            this.setState({ afficher: !this.state.afficher });

            if (this.state.afficher) {
              this.setState({ titre: "afficher" });
            } else {
              this.setState({ titre: "ne pas afficher" });
            }
          }
          }>Click</button>

        {
          this.state.afficher ? <Pictures />
            : null
        }
        <Content />
      </div >

    )
  }
}

export default App;
