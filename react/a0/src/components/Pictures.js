import React, { useEffect, useRef, useState } from 'react';

export default function Pictures() {

    //images dans un tableau
    const [images, setImages] = useState([
        './img/02.jpg',
        './img/nord.jpg',
        './img/nord-2.jpg',
        './img/03.jpg'
    ]);

    const [showButtonClose, setShowButtonClose] = useState(-1);
    const inputToFocus = useRef(null);
    const [image, setImage] = useState(null);

    useEffect(() => inputToFocus.current.focus());

    //créer un composant ici sans créer fichier

    function ImageComponent() {
        // on mappe le tableau des images pour les afficher
        return images.map((nomImage, index) => {
            return (
                <div className="relative"
                    onMouseEnter={() => setShowButtonClose(index)}
                    onMouseLeave={() => setShowButtonClose(-1)}
                >

                    <button className={`bg-white w-5 h-5 pb-1 align-top text-center text-red-500 absolute right-0 flex
                 justify-center item-center rounded-xl focus:outline-none ${index === showButtonClose ? '' : 'hidden'} `}
                        onClick={() => setImages(images.filter((urlImage, i) => i !== index))}  >x</button>
                    <img className="w-40 h-40 mx-auto" src={nomImage} alt="" />
                </div>
            )
        })

    }

    function addImage() {
        let newImages = [image, ...images];
        setImages(newImages);
    }

    function handleImageName(event) {
        setImage(event.target.value);
    }

    return (
        <div>
            <div className='flex item-center justify-between'>
                <ImageComponent />
            </div>

            <div className='mt-5' >
                <input ref={inputToFocus} className='border border-gray-600 shadow rounded p-3 mr-2 outline-none' type='text' onClick={handleImageName} />
                <button className='bg-purple-400 text-white rounded p-3' type='submit' onClick={addImage} >Inscrire le nom de l'image</button>
            </div>
        </div>

    )
}







