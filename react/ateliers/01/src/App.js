import React from "react";
import Logo from "./components/Logo";
import Navigation from "./components/Navigation";

const App = () => {
  return (
    <div>
      <logo />
      <Navigation />

    </div>
  );
}

export default App;
