import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    return (
        <nav className='navigation'>
            <ul>
                <NavLink to="/" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>Acceuil</li>
                </NavLink>

                <NavLink to="/about" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>A propos</li>
                </NavLink>

                <NavLink to="/contact" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>contact</li>
                </NavLink>

                <NavLink to="/connexion" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>Connexion</li>
                </NavLink>
                
                <NavLink to="/test" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>Test</li>
                </NavLink>

                <NavLink to="/blog" className={(nav) => (nav.isActive ? "nav-active" : "")} >
                    <li>Blog</li>
                </NavLink>

            </ul>
        </nav>
    );
};

export default Navigation;