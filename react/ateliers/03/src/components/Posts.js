import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Post from './Post';

const Posts = () => {
    const [data, setData] = useState([]);
    const [searchTitle, setSearchTitle] = useState('');
    const [searchAuteur,  setSearchAuteur ]= useState('');
    useEffect(() => {
        axios.get("https://jsonplaceholder.typicode.com/posts")
            .then((res) => setData(res.data));
    }, []);

    return (
        <div>
            <input className='search'
                type="text" placeholder='taper le titre' onChange={(e) => setSearchTitle(e.target.value)} />

            <input className='search' type="number" min="0" max="9" onwheel="return false;"

                placeholder="entrer titre de l'auteur"
                onChange={(e)=> setSearchAuteur(e.target.value)} />
            <ul>
                {
                    data
                        .filter((post) => post.userId.toString().includes(searchAuteur))
                        .filter((post) => post.title.toLowerCase().includes(searchTitle.toLowerCase()))
                        .map((post) => <Post post={post} key={post.id} />)
                }

            </ul>
        </div>
    );
};

export default Posts;
