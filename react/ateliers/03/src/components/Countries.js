import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Card from './Card';

// https://restcountries.com/v3.1/all

const Countries = () => {

  const [data, setData] = useState([]);
  const radios = ['Africa', 'America', 'Asia', 'Europe', 'Oceania'];
  const [selectedRadio, setSelectedRadio] = useState('');
  const [rangeValue, setRangeValue] = useState(50000000);
  const [searchInput, setSearchInput] = useState("");

  useEffect(() => {
    axios.get("https://restcountries.com/v3.1/all")
      .then((res) => setData(res.data));
  }, []);

  const numberFormat = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  };

  return (
    <div className="countries">
      <ul className='radio-container' >
        {
          radios.map((continent, index) => (
            <li key={index} >
              <input type='radio' name='continent' id={continent}
                onChange={(mavar) => setSelectedRadio(mavar.target.id)}
              />
              <label htmlFor={continent} > {continent} </label>
            </li>

          )
          )
        }
      </ul>
         
        <input className='search'  
          type="text"
          placeholder="entrez le nom d'un pays (en anglais)"
          onChange={ (e)=>setSearchInput(e.target.value) }

        />
      <br />
      <input
        type='range'
        min='0'
        max='1377482166'
        defaultValue={rangeValue}
        onChange={(e) =>  setRangeValue(e.target.value) }
          
      />
      <label id='range' > {numberFormat(rangeValue)} </label>
      <ul>
        {
          data
            .filter( (country)=> country.region.includes(selectedRadio))
            .filter( (country)=> country.population > rangeValue)
            .filter( (country)=> country.name.common.toLowerCase().includes(searchInput.toLowerCase() ) )
            .map((country) => (<Card country={country} pop={numberFormat(country.population)} key={country.name.common} />)
            )}
      </ul>
    </div>
  );
};

export default Countries;