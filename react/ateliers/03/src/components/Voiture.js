import React from 'react';

const Voiture = ({ param,indice }) => {
    return (
        <li className='voiture'>

            <h2 className={(indice===3) ? ('maclass'):('')  }>{param.model}</h2>
            <div className='detail'>


                <div> {param.prix} </div>
            </div>
        </li>
    );
};

export default Voiture;