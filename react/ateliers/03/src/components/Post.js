import React from 'react';

const Post = ({ post }) => {
    return (
        <li className='post'>
            <small>{post.id}</small>
            <h1 className='postinfo'>
                {post.title}
            </h1>
            <p>{post.body}</p>

        </li>
    );
};

export default Post;