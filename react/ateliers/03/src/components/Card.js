import React from 'react';

const Card = ({ country, pop }) => {

  return (
    <li className="card">
      <img src={country.flags.svg} alt="drapeau" />
      <div className="infos">
        <h2>{country.name.common}</h2>
        <h4>{country.capital}</h4>
        <p>Pop. {pop}  </p>
      </div>
    </li>
  );
};

export default Card;
