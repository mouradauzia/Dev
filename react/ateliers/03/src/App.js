import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from "./pages/About";
import Blog from "./pages/Blog";
import Connexion from "./pages/Connexion";
import Contact from "./pages/Contact";
import Home from "./pages/Home";
import Notfound from "./pages/Notfound";
import Test from "./pages/Test";

const App = () => {
  return (
    
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path="/about" element={<About/>} />
        <Route path="/contact" element={<Contact/>} />
        <Route path="/connexion" element={<Connexion/>} />
        <Route path="/test" element={<Test/>} />
        <Route path="*" element={<Notfound/>} />
        <Route path="/blog" element={<Blog/>} />
      </Routes>
    </BrowserRouter>

    
  );
};


export default App;
