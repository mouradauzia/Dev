import React, { useState } from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';
import Voiture from '../components/Voiture';

const Test = () => {
    const [plagePrix, setPlagePrix] = useState(50000);
    const objVoitures = [
        {
            model: "RENAULT",
            prix: 52000
        },
        {
            model: "PEUGEOT",
            prix: 42000
        },
        {
            model: "CITROEN",
            prix: 32000
        },
        {
            model: "FIAT",
            prix: 22000
        },
        {
            model: "VOLKSWAGEN",
            prix: 11000
        },
        {
            model: "MERCEDES",
            prix: 66000
        },
        {
            model: "SKODA",
            prix: 15000
        },
        {
            model: "OPEL",
            prix: 18000
        }
    ];

    return (
        <div>
            <Logo />
            <Navigation />
        <div className='voitures'>
            <ul>
                {
                    objVoitures
                        .filter((voiture) => voiture.prix < plagePrix)
                        .map((elmt) => <Voiture param={elmt} />)
                }
            </ul>
        </div>
            <div className='filtre'>
                <input
                    type="range"
                    min="0"
                    max="500000"
                    defaultValue={plagePrix}
                    onChange={(e) => setPlagePrix(e.target.value)}
                />
            </div>
        </div>
    );
};

export default Test;