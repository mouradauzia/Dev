import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';
import Posts from '../components/Posts';


const Blog = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <Posts/>
        </div>
    );
};

export default Blog;