import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Connexion = () => {
    return (
        <div>
            <Logo/>
            <Navigation/>
        </div>
    );
};

export default Connexion;