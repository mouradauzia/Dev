import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
        <Logo/>
        <Navigation/>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quaerat vel, sit numquam aperiam distinctio! Nemo assumenda aliquam tempore laborum. Voluptates iusto doloribus deserunt obcaecati doloremque, soluta eos laboriosam quaerat minima quod blanditiis ut voluptatibus harum. Sunt error neque, atque natus aperiam laudantium nulla assumenda eaque et culpa voluptatum magnam veniam iusto. Modi odio nemo perspiciatis velit iure sit esse, amet nam quam officiis consectetur explicabo quidem deserunt quisquam sequi dolores commodi ipsum autem at consequatur eos suscipit vitae culpa laborum! Veritatis inventore beatae iste? Nobis accusantium rerum quam minus rem temporibus eveniet, reprehenderit quidem quae labore tenetur asperiores! Assumenda debitis voluptatum maiores mollitia, maxime consequatur nemo accusamus ipsam neque, voluptatibus sed ut aliquid iusto dolor est quae quis tenetur? Alias similique neque vitae! Praesentium fugit molestias eaque vel porro aut, dolores totam doloremque, dicta vitae dolor culpa dignissimos iusto laudantium unde quisquam? Molestiae ad beatae vel veniam exercitationem error aliquam laborum voluptate! Nulla minus veniam esse possimus rerum, provident incidunt voluptates dolore reiciendis? Pariatur similique corrupti eligendi sed doloribus recusandae repudiandae odit perspiciatis nam, ratione sunt reprehenderit illum nemo ad, optio unde maiores beatae voluptate! Enim officia alias nemo labore cumque beatae cupiditate minima, deserunt deleniti nostrum corrupti, distinctio facere aliquid ex omnis doloremque, veritatis fuga aspernatur iste laborum sequi sint laboriosam soluta ut! Sapiente facere eligendi, sint adipisci quod eveniet, voluptatem distinctio ad deserunt delectus qui reiciendis quibusdam officia non eius pariatur molestias hic amet in sed sit optio. Ipsa fuga ut itaque tempore maxime temporibus hic quos!</p>
        </div>
    );
};

export default About;