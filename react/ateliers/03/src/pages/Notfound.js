import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Notfound = () => {
    return (
        <div>
            <Logo/>
            <Navigation/>
            <h1>page non trouvé</h1>
        </div>
    );
};

export default Notfound;