import React from 'react';
import { NavLink } from "react-router-dom";

const Navigateur = () => {
    return (
        <nav>
            <ul>
                <NavLink to="/">
                    <li>Acceuil</li>
                </NavLink>
                <NavLink to="/geographie">
                    <li>Geographie</li>
                </NavLink>
                <NavLink to="/cinema">
                    <li>Cinema</li>
                </NavLink>

                <NavLink to="/contact">
                    <li>Contact</li>
                </NavLink>
            </ul>
        </nav>
    );
};

export default Navigateur;