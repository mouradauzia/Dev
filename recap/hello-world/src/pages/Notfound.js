import React from 'react';
import Logo from '../components/Logo';
import Navigateur from '../components/Navigateur';

const Notfound = () => {
    return (
        <div>
            <Logo />
            <Navigateur />
            <h1>erreur404</h1>
        </div>
    );
};

export default Notfound;