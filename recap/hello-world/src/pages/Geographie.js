import React from 'react';
import Logo from '../components/Logo';
import Navigateur from '../components/Navigateur';
import Pays from '../components/Pays';

const Geographie = () => {
    return (
        <div>
           <Logo />
           <Navigateur />
           <h1>geographie</h1>
           <Pays />            
        </div>
    );
};

export default Geographie;