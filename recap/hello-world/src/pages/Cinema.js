import React from 'react';
import Logo from '../components/Logo';
import Navigateur from '../components/Navigateur';

const Cinema = () => {
    return (
        <div>
            <Logo />
            <Navigateur />
        </div>
    );
};

export default Cinema;