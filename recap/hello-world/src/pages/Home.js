import React from 'react';
import Logo from '../components/Logo';
import Navigateur from '../components/Navigateur';

const Home = () => {
    return (
        <div>
            <Logo />
            <Navigateur />
        </div>
    );
};

export default Home;