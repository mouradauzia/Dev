import React from 'react';
import Logo from '../components/Logo';
import Navigateur from '../components/Navigateur';

const Contact = () => {

    function sayHello(){
        alert("hello world");
    }
    
    return (
        <div>
            <Logo />
            <Navigateur />
            <div>
                <button onClick={sayHello}>click</button>
            </div>
        </div>
    );
};

export default Contact;